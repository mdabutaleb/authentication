@if($errors->any())

    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <p></p>  {{ $error }} </p>
        @endforeach
    </div>

@endif